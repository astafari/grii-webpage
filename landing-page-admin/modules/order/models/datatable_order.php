<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Datatable_order extends CI_Model {
 
    var $table = 'trx_order';
    var $column_order = array(null, 'order_date','order_code','customer_name','plat_nomor','driver_name','tujuan','order_amount','order_status'); //set column field database for datatable orderable
    var $column_search = array('order_date','order_code','customer_name','plat_nomor','driver_name','tujuan','order_amount','order_status'); //set column field database for datatable searchable 
    var $order = array('order_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
    }
 
    private function _get_datatables_query()
    {
         
        $this->db->select('trx_order.*,master_armada.plat_nomor, master_customer.customer_name, master_tujuan.tujuan, master_driver.driver_name');
        $this->db->from($this->table);
		$this->db->join('master_armada','master_armada.armada_id=trx_order.order_armada','LEFT');
		$this->db->join('master_customer','master_customer.customer_id=trx_order.order_customer','LEFT');
		$this->db->join('master_tujuan','master_tujuan.tujuan_id=trx_order.order_destination','LEFT');
		$this->db->join('master_driver','master_driver.driver_id=trx_order.order_driver','LEFT');
        $i = 0;
		
			$str_like = "( ";
			$i=0;
			foreach ($this->column_search as $item){ // loop column 
					$str_like .= $i!=0?"OR":"";
					$str_like .=" ".$item." LIKE '%".$_POST['search']['value']."%' ";			
					$i++;
			}
			$str_like .= " ) ";
			$this->db->where($str_like);     
//			$this->db->limit(100);
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}