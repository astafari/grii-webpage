<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo themeUrl();?>assets/images/favicon.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo themeUrl();?>assets/images/favicon.png" type="image/x-icon"/>
    <title>GRII - ADMIN LANDING PAGE</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/fontawesome.css">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/icofont.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/themify.css">

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/flag-icon.css">

    <!-- prism css -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/prism.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/bootstrap.css">

    <!-- vertical-menu css -->
    <link  rel="stylesheet" type="text/css"  href="<?php echo themeUrl();?>assets/css/vertical-menu.css">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>assets/css/datatables.css" />

</head>
<body>

<!-- Loader starts -->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <h4>GRII - ADMIN LANDING PAGE</h4>
    </div>
</div>
<!-- Loader ends -->

<div class="page-wrapper">

    <!--Page Header Start-->
    <div class="page-main-header">
        <div class="main-header-left">
            <div class="logo-wrapper">
                <a href="index.html">
                    <img src="<?php echo themeUrl();?>assets/images/logo-light.png" class="image-dark" alt=""/>
                    <img src="<?php echo themeUrl();?>assets/images/logo-light-dark-layout.png" class="image-light" alt=""/>
                </a>
            </div>
        </div>
        <div class="main-header-right row">
            <div class="vertical-mobile-sidebar">
                <i class="fa fa-bars sidebar-bar"></i>
            </div>
            <div class="nav-right col">
                <ul class="nav-menus">
                    <li>
                        <a href="#!" onclick="javascript:toggleFullScreen()" class="text-dark">
                            <img class="align-self-center pull-right mr-2" src="<?php echo themeUrl();?>assets/images/dashboard/browser.png" alt="header-browser">
                        </a>
                    </li>
                    <li class="onhover-dropdown">
                        <div class="media  align-items-center">
                            <img class="align-self-center pull-right mr-2" src="<?php echo themeUrl();?>assets/images/dashboard/user.png" alt="header-user"/>
                            <div class="media-body">
                                <h6 class="m-0 txt-dark f-16">
                                    My Account
                                    <i class="fa fa-angle-down pull-right ml-2"></i>
                                </h6>
                            </div>
                        </div>
                        <ul class="profile-dropdown onhover-show-div p-20">
                            <li>
                                <a href="#">
                                    <i class="icon-user"></i>
                                    Edit Profile
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('meme/me/change_password');?>">
                                    <i class="icon-user"></i>
                                    Change Password
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('auth/out');?>" class="act_confirm" data-title="Logout" data-body="Apakah anda yakin akan logout ?" data-desc="Tekan Tidak jika anda ingin melanjutkan pekerjaan anda. Tekan Ya untuk keluar." data-icon="fa-sign-out">
                                    <i class="icon-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="d-lg-none mobile-toggle">
                    <i class="icon-more"></i>
                </div>
            </div>
        </div>
    </div>
    <!--Page Header Ends-->

    <!--vertical menu start-->
    <div class="vertical-menu-main">
        <nav id="main-nav">
            <!-- Sample menu definition -->
            <ul id="main-menu" class="sm pixelstrap">
				<li>
                    <div class="text-right mobile-back">
                        Back<i class="fa fa-angle-right pl-2" aria-hidden="true"></i>
                    </div>
                </li>
				<?php top_menu($this->jCfg['menu']);?>   
            </ul>
        </nav>
    </div>
    <!--vertical menu ends-->

    <div class="page-body-wrapper rtl">

        <div class="page-body vertical-menu-mt">

            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <?php get_breadcrumb($this->breadcrumb);?>
                    </div>
                </div>
            </div>
			<div class="container-fluid">
                <?php if($this->is_dashboard==FALSE){?>
				<div class="row">
                    <div class="col-sm-12">
                        <div class="card" style="padding:10px;">
                                    <div class="col-sm-9 offset-sm-3">
										<?php isset($links)?getLinksWebArch($links):'';?>                                         
                                    </div>
                            </div>
                        </div>
                    </div>
                <?php }?>

			