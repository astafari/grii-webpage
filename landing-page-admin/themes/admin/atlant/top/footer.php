        </div>
    </div>
</div>

<!-- latest jquery-->
<script src="<?php echo themeUrl();?>assets/js/jquery-3.2.1.min.js" ></script>

<!-- Bootstrap js-->
<script src="<?php echo themeUrl();?>assets/js/bootstrap/popper.min.js" ></script>
<script src="<?php echo themeUrl();?>assets/js/bootstrap/bootstrap.js" ></script>

<!-- prism js -->
<script src="<?php echo themeUrl();?>assets/js/prism/prism.min.js"></script>

<!-- clipboard js -->
<script src="<?php echo themeUrl();?>assets/js/clipboard/clipboard.min.js" ></script>

<!-- custom card js  -->
<script src="<?php echo themeUrl();?>assets/js/custom-card/custom-card.js" ></script>
<!--Datatable js-->
<script src="<?php echo themeUrl();?>assets/js/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo themeUrl();?>assets/js/datatables/datatable.custom.js"></script>

<!-- Theme js-->
<script src="<?php echo themeUrl();?>assets/js/script.js" ></script>

<!-- SmartMenus jQuery plugin -->
<script  src="<?php echo themeUrl();?>assets/js/vertical-menu.js"></script>

<!--drilldown menu-->
<script src="<?php echo themeUrl();?>assets/js/jquery.drilldown.js"></script>

<!--Mega menu menu-->
<script src="<?php echo themeUrl();?>assets/js/megamenu.js"></script>

</body>
</html>