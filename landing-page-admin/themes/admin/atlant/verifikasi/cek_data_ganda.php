<div class="panel panel-default" style="margin-top:-10px;">
    <div class="panel-body panel-body-table">
        <div class="table-responsive">
            <table class="table table-striped datatable">
               <thead>
                <tr>
                    <th width="30px">No</th>
                    <th width="30px">NIK</th>
                    <th width="100px">NPAPG</th>
                    <th width="100px">Nama Lengkap</th>
                    <th width="100px">Tempat Lahir</th>
                    <th width="100px">Tanggal Lahir</th>
                    <th width="100px">DPD Pengusul</th>
                    <th width="50px">Status Data</th>
                    <th width="50px">Action</th>
                </tr>
                </thead>
               <tbody> 
                <?php 
                if(count($data) > 0){
					$no=1;
                    foreach($data as $r){
                        ?>
                        <tr>
                            <td><?php echo $no++;?></td>
                            <td><?php echo $r->kta_no_id;?></td>
                            <td><?php echo $r->kta_nomor_kartu;?></td>
                            <td><?php echo $r->kta_nama_lengkap;?></td>
                            <td><?php echo $r->kta_tempat_lahir;?></td>
                            <td><?php echo $r->kta_tgl_lahir;?></td>
                            <td><?php echo $r->nama_pengguna;?></td>
                            <td>
							<?php if($r->kta_status_data == 0 ){
									echo '<span class="label label-warning" data-toggle="tooltip" data-placement="top" title data-original-title="Waiting to Approve"><li class="fa fa fa-spinner"></li> Pending</span>';								
								  }elseif($r->kta_status_data == 1 ){
									echo '<span class="label label-success" data-toggle="tooltip" data-placement="top" title data-original-title="Approved"><li class="fa fa-check-circle"></li> Approved</span>';								
								  }elseif($r->kta_status_data == 2 ){
									echo '<span class="label label-info" data-toggle="tooltip" data-placement="top" title data-original-title="Waiting to Entry"><li class="fa fa fa-spinner"></li> Uploaded</span>';								
								  }elseif($r->kta_status_data == 3){
									echo '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Rejected by Operator Entry"><li class="glyphicon glyphicon-ban-circle"></li> Rejected</span>';								
								  }elseif($r->kta_status_data == 4){
									echo '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Rejected by Koordinator Data"><li class="glyphicon glyphicon-ban-circle"></li> Rejected</span>';								
								  }elseif($r->kta_status_data == 9){
									echo '<span class="label label-info" data-toggle="tooltip" data-placement="top" title data-original-title="Data Event"><li class="glyphicon glyphicon-ban-circle"></li> Event</span>';								
								  }else{
									echo '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Rejected"><li class="glyphicon glyphicon-ban-circle"></li></span>';								
								  }
							?></td>
                            <td>
								<a href="<?php echo $own_links."/edit/?_id="._encrypt($r->kta_id);?>"><span class="label label-default label-form" data-toggle="tooltip" data-placement="top" title data-original-title="See Detail"><li class="fa fa-list-alt"></li></span></a>	                                
								<a href="<?php echo $own_links."/delete/?_id="._encrypt($r->kta_id);?>" class='tip act_confirm' rel='600|150' data-title='Konfirmasi Hapus Data' data-icon='fa-trash-o' data-desc='Pastikan data yang anda akan hapus ini benar, agar tidak terjadi kesalahan.' data-body='Apakah anda yakin data akan dihapus ??'><span class="label label-danger label-form" data-toggle="tooltip" data-placement="top" title data-original-title="Hapus Data Ini"><li class="glyphicon glyphicon-trash"></li></span></a>		
                            </td>
                        </tr>
                <?php } 
                }
                ?>
                </tbody>
            </table>
            
        </div>
    </div>
</div>

    