<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title><?php echo cfg('app_name');?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo themeUrl();?>js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo themeUrl();?>js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo themeUrl();?>js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
        <script type="text/javascript">
            var BASE_URL = '<?php echo base_url();?>';  
            var THEME_URL = '<?php echo themeUrl();?>';  
            var CURRENT_URL = '<?php echo current_url();?>';
            var MEME = {};
        </script>

        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo themeUrl();?>css/theme-serenity-head-light.css"/>
        <?php load_css();?>

        <!-- EOF CSS INCLUDE -->                                     
    </head>
    <body>
	
        <?php get_info_message();?>
        <!-- START PAGE CONTAINER -->
        <div class="page-container page-navigation-top">            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal">
                    <li class="xn-logo">
                        <a href="index.html">ATLANT</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <?php if($this->jCfg['theme_setting']['header']==true){?>
                    <li class="xn-navigation-control">
                        <a href="<?php echo site_url('meme/me');?>" class="x-navigation-control"></a>
                    </li>
                    <?php }else{ ?>
                    <li class="xn-logo">
                        <img src="<?php echo themeUrl();?>img/logo.png" height="40" />
                        <a href="<?php echo site_url('meme/me');?>" class="x-navigation-control"></a>
                    </li>
                    <?php } ?>
                    <?php top_menu($this->jCfg['menu']);?>
                    <!-- POWER OFF -->
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                            <li><a href="<?php echo site_url('meme/me/change_password');?>"><span class="fa fa-key"></span> Change Password</a></li>
                            <li><a href="<?php echo site_url('auth/out');?>" class="act_confirm" data-title="Logout" data-body="Apakah anda yakin akan logout ?" data-desc="Tekan Tidak jika anda ingin melanjutkan pekerjaan anda. Tekan Ya untuk keluar." data-icon="fa-sign-out"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                    <!-- END POWER OFF -->   
                    <!--<li class="xn-search pull-right">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>-->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <?php get_breadcrumb($this->breadcrumb);?>
                <!-- END BREADCRUMB -->                
                <div class="row">
                    <div class="col-md-6">
                        <div class="page-title">                    
                            <h2><span class="fa fa-arrow-circle-o-left"></span> <?php echo isset($title)?$title:'';?></h2>
                        </div>   
                    </div>   
                    <div class="col-md-6">
                        <!-- START TABS -->                                
                        <div class="panel panel-default tabs" style="border-top-width:0px;">   
                        <?php if($this->is_dashboard==FALSE){?>
                                                 
                            
                            <ul class="nav nav-tabs pull-right" role="tablist">
                                <?php isset($links)?getLinksWebArch($links):'';?> 
                            </ul>                            
                            
                              
                        <?php }else{ ?>

                            <ul class="nav nav-tabs pull-right" role="tablist" style="margin-top:-30px;">

                            </ul>

                        <?php } ?>  
                         </div>                                                   
                        <!-- END TABS -->               
                    </div>  
                    <div style="clear:both;"></div>
                    <div style="border-bottom:1px solid #009F9A;margin:-20px 10px 10px 10px;" id="border-header"></div>           
                </div>
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row" style="margin:-20px 10px 10px 10px;">
                        <div class="col-md-12 panel" id="panel-content-wrap" style="border-radius:0px;padding:20px;">


