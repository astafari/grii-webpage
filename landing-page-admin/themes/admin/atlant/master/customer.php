<?php js_validate(); ?>
                <div class="row">			
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Data Customer</h5>
                                <span>Data Customer Terdaftar CV - Anugerah Tanjung Permai.</span>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive pull-left">
									<table id="table-cust" class="display">
									   <thead align="center">
										<tr>
											<th width="2px">No</th>
											<th>Nama</th>
											<th>PIC</th>
											<th>No. Hp</th>
											<th>Email</th>
											<th>Alamat</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
										</thead>
									   <tbody align="center">
									   </tbody>
									   <tfoot align="center">
										<tr>
											<th width="2px">No</th>
											<th>Nama</th>
											<th>PIC</th>
											<th>No. Hp</th>
											<th>Email</th>
											<th>Alamat</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
										</tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
				</div>
<script type="text/javascript">
	var AUTH_URL = '<?php echo $own_links;?>/ajax_list';
    var URL_AJAX = '<?php echo base_url();?>index.php/ajax/data';
</script>				

				