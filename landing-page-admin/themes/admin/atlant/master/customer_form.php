<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/save" class="form-horizontal" method="post"> 
        <input type="hidden" name="customer_id" id="customer_id" value="<?php echo isset($val->customer_id)?$val->customer_id:'';?>" />
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Input Data Customer</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Nama</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan Nama Customer / Nama PT. / Nama Instansi" name="customer_name" value="<?php echo isset($val)?$val->customer_name:"";?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Nama PIC</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan Nama PIC " name="customer_pic_name" value="<?php echo isset($val)?$val->customer_pic_name:"";?>">
                                                </div>
                                            </div>											
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Status</label>
                                                <div class="col-sm-9">
                                                <select class="form-control form-control digits" id="exampleFormControlSelect29" name="customer_status">
                                                    <?php foreach((array)cfg('status') as $kj=>$vj){
															$s = isset($val)&&$val->customer_status==$kj?'selected="selected"':'';
															echo "<option value='".$kj."' $s >".$vj."</option>";
													} ?>
                                                </select>
                                                </div>
                                            </div>												
                                        </div>
                                    </div>
                                </div>							
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Input Data Tambahan</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Alamat</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan Alamat Customer / Alamat Kantor " name="customer_address" value="<?php echo isset($val)?$val->customer_address:"";?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">No. HP</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan No. HP Customer / No. Telp Kantor " name="customer_hp" value="<?php echo isset($val)?$val->customer_hp:"";?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan Email Customer / Email Kantor " name="customer_email" value="<?php echo isset($val)?$val->customer_email:"";?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>								
                        </div>
                    </div>					
				</div>