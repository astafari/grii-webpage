<?php js_validate(); ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
							<form id="form-validated" action="<?php echo $own_links;?>/search" method="post" >
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                <select class="form-control" id="propinsi" name="propinsi">
                                                    <option>- Pilih Propinsi - </option>
													<?php foreach ((array)get_propinsi() as $m) {
														$s = isset($param['propinsi'])?($param['propinsi']==$m->propinsi_kode?'selected="selected"':''):'';
														echo "<option value='".$m->propinsi_kode."' $s >".$m->propinsi_nama."</option>";
													}?>
												</select>
                                                </div>
                                                <div class="col-sm-3">
													<select class="form-control" id="kabupaten" name="kabupaten" data-live-search="true">
														<option value=""> - kabupaten/kota - </option>
													</select>
                                                </div>												
                                                <div class="col-sm-3">
													<select class="form-control" id="kecamatan" name="kecamatan" data-live-search="true">
														<option value=""> - kecamatan - </option>
													</select>
                                                </div>
                                                <div class="col-sm-3">
													<select class="form-control" id="kelurahan" name="kelurahan" data-live-search="true">
														<option value=""> - kelurahan - </option>
													</select>
                                                </div>												
											</div>
                                        </div>										
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                <select class="form-control digits" id="exampleFormControlSelect9">
                                                    <option>- Pilih Jenis Kelamin -</option>
                                                </select>
                                                </div>
                                                <div class="col-sm-3">
                                                <select class="form-control digits" id="exampleFormControlSelect9">
                                                    <option>- Pilih Umur -</option>
                                                </select>
                                                </div>												
                                                <div class="col-sm-3">
                                                <select class="form-control digits" id="exampleFormControlSelect9">
                                                    <option>- Pilih TPS -</option>
                                                </select>
                                                </div>
                                                <div class="col-sm-3">
                                                <input type="text" class="form-control" placeholder="search by nama / alamat / tempat lahir">
                                                </div>												
											</div>
                                        </div>										
                                    </div>									
                                </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                        <input type="reset" class="btn btn-light" value="Cancel">
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>				
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>DPT PEMILU 2019</h5>
                                <span>Daftar Pemilih Tetap Pemilihan Umum 2019.</span>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
									<table id="table-dpt" class="display datatables">
									   <thead>
										<tr>
											<th width="30px">No</th>
											<th>NKK</th>
											<th>NIK</th>
											<th>Nama Lengkap</th>
											<th>Tempat, Tgl Lahir</th>
											<th>Umur</th>
											<th>Jenis Kelamin</th>
											<th>Difabel</th>
											<th>TPS</th>
											<th>Alamat</th>
											<th>Kelurahan</th>
											<th>Kecamatan</th>
											<th>Kabupaten</th>
											<th>Propinsi</th>
										</tr>
										</thead>
									   <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
				</div>
<script type="text/javascript">
	var AUTH_URL = '<?php echo $own_links;?>/ajax_list';
    var URL_AJAX = '<?php echo base_url();?>index.php/ajax/data';
</script>				

				