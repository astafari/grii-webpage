<?php
	$approve = "";
	if(!empty($filter) && $filter == "2"){
		foreach ((array)get_report_dpd() as $p => $q) {
			$koma = $p>0?',':'';
			$approve = $approve.$koma."['".$q->nama."',".$q->jumlah."]";
			$judul = "STATISTIK DATA KTA PER DPD PROVINSI / DAERAH";
		}
	}elseif($filter == "3"){
		foreach ((array)get_report_upload() as $p => $q) {
			$koma = $p>0?',':'';
			$upload = $upload.$koma."['".$q->nama."',".$q->upload."]";
			$entry = $entry.$koma."['".$q->nama."',".$q->entry."]";
			$judul = "STATISTIK DATA UPLOAD & ENTRY PER DPD PROVINSI / DAERAH";
		}
	}elseif($filter == "4"){
		foreach ((array)get_report_jk() as $p => $q) {
			$koma = $p>0?',':'';
			$laki = $laki.$koma."['".$q->nama."',".$q->laki."]";
			$perempuan = $perempuan.$koma."['".$q->nama."',".$q->perempuan."]";
			$judul = "STATISTIK DATA LAKI-LAKI DAN PEREMPUAN PER PROVINSI / DAERAH";
		}
//		debugCode(get_report_jk());
	}else{
		foreach ((array)get_cart_prop() as $p => $q) {
			$koma = $p>0?',':'';
			$approve = $approve.$koma."['".$q->nama."',".$q->jumlah."]";
			$judul = "STATISTIK DATA KTA PER PROVINSI";
		}				
	}
//debugCode($upload);
js_hight_chart();
$jenkel = cfg('jenkel');
$status = cfg('status_data');
?>
<div class="panel panel-default tabs">                            
	<ul class="nav nav-tabs" role="tablist">
	    <li><a href="<?php echo site_url()?>" role="tab">Dashboard Maps Distribution</a></li>
	    <li class="active"><a href="<?php echo site_url('meme/me/detail_kta')?>" role="tab">Statistik Data Anggota</a></li>
	    <li><a href="<?php echo site_url('meme/me/detail_kta')?>" role="tab">Grafik Data Anggota</a></li>
	    <li><a href="<?php echo site_url('meme/me/verifikasi')?>" role="tab">Cari Data Anggota</a></li>
	</ul>                            
	<div class="panel-body tab-content">
	    <div class="tab-pane active">                            
    <form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/detail_kta" class="form-horizontal" method="post"> 
        <input type="hidden" name="kta_id" id="kta_id" value="<?php echo isset($val->kta_id)?$val->kta_id:'';?>" />

	  <div class="well">
	  			<div class="col-md-9">
					<select class="form-control select" id="filter" name="filter" data-live-search="true">
						<option value=""> - pilih filter laporan- </option>
						<option value="1" <?php echo $filter=="1"?"selected":""; ?> > Lihat Laporan Berdasarkan Provinsi </option>
						<option value="2" <?php echo $filter=="2"?"selected":""; ?>> Lihat Laporan Berdasarkan DPD Provinsi </option>
						<option value="3" <?php echo $filter=="3"?"selected":""; ?>> Lihat Laporan Upload & Entry Berdasarkan DPD Provinsi </option>
						<option value="4" <?php echo $filter=="4"?"selected":""; ?>> Lihat Laporan Statistik Perempuan & Laki-laki Berdasarkan Provinsi </option>
					</select>
	  			</div>
	  		<div class="row">	  			
				<div class="col-md-2" style="margin-top:0px;">
					<input style="margin-right:5px;" name="btn_search"  class="btn btn-primary" type="submit" value="Load Report">
	  			</div>
	  		</div>	  	
	  </div>        <div class="panel-body">                                                                        
    </form>
            <div class="row">
					<?php if($filter == "3"){ ?>
					<div class="col-md-12">
						<div class="panel" id="cart_bar_upload" style="height: 250px;">
					</div>
					</div>
					<div class="col-md-12">
						<div class="panel" id="cart_bar_entry" style="height: 250px;">
					</div>
					</div>
					<?php }elseif($filter == "4"){ ?>
					<div class="col-md-12">
						<div class="panel" id="cart_bar_laki" style="height: 250px;">
					</div>
					</div>
					<div class="col-md-12">
						<div class="panel" id="cart_bar_perempuan" style="height: 250px;">
					</div>
					</div>
					<?php }else{ ?>
					<div class="col-md-12">
						<div class="panel" id="cart_bar_by_prov" style="height: 450px;">
					</div>
					</div>
					<?php } ?>
			 </div>                           			 
            <div class="row">
			<?php if($filter == "3"){ ?>
				<div class="col-md-6">
            		<h5 class="heading-form"># Grafik Status Data KTA Partai Golkar Seluruh Indonesia</h5>       
					<br />
						<div class="panel" id="cart_pie_status" style="height: 300px;"></div>
				</div>
			<?php }elseif($filter == "4"){ ?>
				<div class="col-md-6">
            		<h5 class="heading-form"># Grafik Jumlah Anggota Perempuan & Laki-laki Partai Golkar Seluruh Indonesia</h5>       
					<br />
						<div class="panel" id="cart_pie_gender" style="height: 300px;"></div>
				</div>
			<?php }else{ ?>
				<div class="col-md-6">
            		<h5 class="heading-form"># Pertumbuhan Data Anggota KTA-PG</h5>       
            		<div class="row">       
						<div class="col-md-2 control-label">Periode</div>
						<div class="col-md-5">
							<input type="text" name="date_from" id="date_from"  class="form-control datepicker" placeholder="Tanggal Awal" maxlength="16" onchange="getGrowingData('<?php echo $filter;?>',$(this).val(),$('#date_to').val())"/>
						</div>
						<div class="col-md-5">
							<input type="text" name="date_to" id="date_to"  class="form-control datepicker" placeholder="Tanggal Akhir" maxlength="16" onchange="getGrowingData('<?php echo $filter;?>',$('#date_from').val(),$(this).val())"/>
						</div>      
					</div>
					<br />
					<div class="panel-body panel-body-table" style="height: 300px; overflow-y: scroll;">			
				        <div class="table-responsive">
				            <table class="table table-hover table-bordered" id="dash_two">
				               <thead>
				                <tr>
				                    <th>Keterangan</th>
				                    <th>Jumlah</th>
				                </tr>
				                </thead>
				               <tbody>
				                </tbody>
				            </table>
			            </div>
				    </div>					
				</div>
			<?php } ?>
			<?php if($filter == "3"){ ?>
				<div class="col-md-6">
            		<h5 class="heading-form"># Pertumbuhan Data Anggota KTA-PG Berdasarkan Status Data</h5>       
            		<div class="row">       
						<div class="col-md-2 control-label">Pengusul</div>
						<div class="col-md-8">		  			
							<select class="form-control select" id="area" name="area" data-live-search="true" onchange="getStatusData($('#area').val())">
								<option value=""> - cari berdasarkan pengusul / dpd- </option>
								<?php 
									$st = 0;
									foreach ((array)get_pemesan($st) as $m) {
										echo "<option value='".$m->penggunaID."'>".$m->nama_pengguna."</option>";
									}
								?>							
								</select>
						</div>
					</div>
					<br />
					<div class="panel-body panel-body-table" style="height: 300px; overflow-y: scroll;">			
				        <div class="table-responsive">
				            <table class="table table-hover table-bordered table-striped" id="dash_five">
				               <thead>
				                <tr>
				                    <th width="30px">No</th>
				                    <th>Propinsi</th>
				                    <th>Upload</th>
				                    <th>Reject Upload</th>
				                    <th>Entry</th>
				                    <th>Reject Entry</th>
				                    <th>Approve</th>
				                </tr>
				                </thead>
				               <tbody>
				                </tbody>
				            </table>
			            </div>
				    </div>					
				</div>
			<?php }elseif($filter == "4"){ ?>
				<div class="col-md-6">
            		<h5 class="heading-form"># Pertumbuhan Data Anggota KTA-PG Berdasarkan Jenis Kelamin</h5>       
            		<div class="row">       
						<div class="col-md-2 control-label">Propinsi</div>
						<div class="col-md-8">		  			
							<select class="form-control select" id="area" name="area" data-live-search="true" onchange="getGenderData($('#area').val())">
								<option value=""> - propinsi - </option>
								<?php foreach ((array)get_propinsi() as $m) {
									echo "<option value='".$m->propinsi_kode."'>".$m->propinsi_nama."</option>";
								}?>
							</select>
						</div>
					</div>
					<br />
					<div class="panel-body panel-body-table" style="height: 300px; overflow-y: scroll;">			
				        <div class="table-responsive">
				            <table class="table table-hover table-bordered table-striped" id="dash_four">
				               <thead>
				                <tr>
				                    <th width="30px">No</th>
				                    <th>Propinsi</th>
				                    <th>Laki-laki</th>
				                    <th>Perempuan</th>
				                </tr>
				                </thead>
				               <tbody>
				                </tbody>
				            </table>
			            </div>
				    </div>					
				</div>
			<?php }else{ ?>
				<div class="col-md-6">
            		<h5 class="heading-form"># Pertumbuhan Data Anggota KTA-PG</h5>       
            		<div class="row">       
						<?php if($filter == "2"){ ?>
						<div class="col-md-2 control-label">Pengusul</div>
						<div class="col-md-8">
							<select class="form-control select" id="area" name="area" data-live-search="true" onchange="getAreaData('<?php echo $filter;?>',$('#area').val())">
								<option value=""> - cari berdasarkan pengusul / dpd- </option>
								<?php 
									$st = 0;
									foreach ((array)get_pemesan($st) as $m) {
										echo "<option value='".$m->penggunaID."'>".$m->nama_pengguna."</option>";
									}
								?>
							</select>
						</div>
						<?php }else{ ?>
						<div class="col-md-2 control-label">Provinsi</div>
						<div class="col-md-8">		  			
							<select class="form-control select" id="area" name="area" data-live-search="true" onchange="getAreaData('<?php echo $filter;?>',$('#area').val())">
								<option value=""> - provinsi - </option>
								<?php foreach ((array)get_propinsi() as $m) {
									echo "<option value='".$m->propinsi_kode."'>".$m->propinsi_nama."</option>";
								}?>
							</select>
						</div>
						<?php } ?>
					</div>
					<br />
					<div class="panel-body panel-body-table" style="height: 300px; overflow-y: scroll;">			
				        <div class="table-responsive">
				            <table class="table table-hover table-bordered table-striped" id="dash_three">
				               <thead>
				                <tr>
				                    <th width="30px">No</th>
				                    <th><?php echo $filter=="2"?"DPD Provinsi":"Propinsi";?></th>
				                    <th>Jumlah</th>
				                    <th>Target</th>
				                </tr>
				                </thead>
				               <tbody>
				                </tbody>
				            </table>
			            </div>
				    </div>					
				</div>
			<?php } ?>
                </div>
			 </div>   			 
        </div>
        </div>
<script type="text/javascript">
var AJAX_URL = '<?php echo site_url("ajax/data");?>';
$(document).ready(function(){
	getGrowingData('<?php echo $filter; ?>','','<?php echo date('Y-m-d')?>');
	getAreaData('<?php echo $filter; ?>','');
	getGenderData('');
	getStatusData('');
}); 
function getGenderData(area) {
    $('#dash_four tbody').html('<tr><td colspan="4" style="text-align: center">Mohon Tunggu...</td></tr>');
	$.post(AJAX_URL+"/get_gender_data",{area:area},function(o){
			$('#dash_four tbody').html(o);
	});		
}
function getGrowingData(filter,awal,akhir) {
    $('#dash_two tbody').html('<tr><td colspan="2" style="text-align: center">Mohon Tunggu...</td></tr>');
	$.post(AJAX_URL+"/get_growing_data",{filter:filter,awal:awal,akhir:akhir},function(o){
			$('#dash_two tbody').html(o);
	});		
}
function getAreaData(filter,area) {
    $('#dash_three tbody').html('<tr><td colspan="4" style="text-align: center">Mohon Tunggu...</td></tr>');
	$.post(AJAX_URL+"/get_area_data",{filter:filter,area:area},function(o){
			$('#dash_three tbody').html(o);
	});		
}
function getStatusData(area) {
    $('#dash_five tbody').html('<tr><td colspan="7" style="text-align: center">Mohon Tunggu...</td></tr>');
	$.post(AJAX_URL+"/get_status_data",{area:area},function(o){
			$('#dash_five tbody').html(o);
	});		
}
$(function () {
    $('#cart_pie_status').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
        },
        colors:['#009F9A','#38B8E3','#90B356'],
        title: {
            text: ''
        },
        subtitle: {
//            text: '<b>Anggota Berdasarkan Jenis Kelamin</b>'
        },
        tooltip: {
		   formatter: function() {
			  return '<b>'+ this.point.name +'</b>: '  + this.percentage.toFixed(2) + ' %' ;
		   }
        },
        credits: {
            enabled:false
        },
        exporting:{
            enabled:false
        },
        plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
        series: [{
            type: 'pie',
            name: 'Status Data',
            data: [
                <?php 
                foreach ((array)get_pie_status() as $k => $v) {
                   echo $k==0?"":",";
                   echo "['".$status[$v->nama]."',".$v->jumlah."]";
                }?>
            ]
        }]
    });

    $('#cart_pie_gender').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
        },
        colors:['#009F9A','#38B8E3','#90B356'],
        title: {
            text: ''
        },
        subtitle: {
//            text: '<b>Anggota Berdasarkan Jenis Kelamin</b>'
        },
        tooltip: {
		   formatter: function() {
			  return '<b>'+ this.point.name +'</b>: '  + this.percentage.toFixed(2) + ' %' ;
		   }
        },
        credits: {
            enabled:false
        },
        exporting:{
            enabled:false
        },
        plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
        series: [{
            type: 'pie',
            name: 'Jenis Kelamin',
            data: [
                <?php 
                foreach ((array)get_pie_gender() as $k => $v) {
                   echo $k==0?"":",";
                   echo "['".$jenkel[$v->nama]."',".$v->jumlah."]";
                }?>
            ]
        }]
    });

    //line pertumbuhan anggota
    $('#cart_bar_by_prov').highcharts({
        chart: {
            type: 'column',
            marginLeft:60,
            marginRight:10,
            reflow: true
        },
        title: {
            text: ''
        },
        credits: {
          enabled:false
        },
        exporting:{
          enabled:false
        },
        subtitle: {
            text: '<b><?php echo $judul;?></b>'
        },
        plotOptions: {
            column : {
                pointWidth:23
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -35,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:f}</b>'
        },
        series: [{
            name: 'Jumlah Data',
            data: [<?php echo $approve;?>],
            dataLabels: {
                enabled: true,
                color: '#111',
                align: 'center',
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            color:'#02dbd0'
        }]
    });
	
    $('#cart_bar_upload').highcharts({
        chart: {
            type: 'column',
            marginLeft:60,
            marginRight:10,
            reflow: true
        },
        title: {
            text: ''
        },
        credits: {
          enabled:false
        },
        exporting:{
          enabled:false
        },
        subtitle: {
            text: '<b>GRAFIK DATA UPLOAD PER DPD PROVINSI PARTAI GOLKAR</b>'
        },
        plotOptions: {
            column : {
                pointWidth:23
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -35,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:f}</b>'
        },
        series: [{
            name: 'Jumlah Data',
            data: [<?php echo $upload;?>],
            dataLabels: {
                enabled: true,
                color: '#111',
                align: 'center',
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            color:'#02dbd0'
        }]
    });
	
    $('#cart_bar_entry').highcharts({
        chart: {
            type: 'column',
            marginLeft:60,
            marginRight:10,
            reflow: true
        },
        title: {
            text: ''
        },
        credits: {
          enabled:false
        },
        exporting:{
          enabled:false
        },
        subtitle: {
            text: '<b>GRAFIK DATA ENTRY PER DPD PROVINSI PARTAI GOLKAR</b>'
        },
        plotOptions: {
            column : {
                pointWidth:23
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -35,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:f}</b>'
        },
        series: [{
            name: 'Jumlah Data',
            data: [<?php echo $entry;?>],
            dataLabels: {
                enabled: true,
                color: '#111',
                align: 'center',
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            color:'#02dbd0'
        }]
    });

    $('#cart_bar_laki').highcharts({
        chart: {
            type: 'column',
            marginLeft:60,
            marginRight:10,
            reflow: true
        },
        title: {
            text: ''
        },
        credits: {
          enabled:false
        },
        exporting:{
          enabled:false
        },
        subtitle: {
            text: '<b>STATISTIK DATA LAKI-LAKI PER PROVINSI</b>'
        },
        plotOptions: {
            column : {
                pointWidth:23
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -35,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:f}</b>'
        },
        series: [{
            name: 'Jumlah Data Laki-laki',
            data: [<?php echo $laki;?>],
            dataLabels: {
                enabled: true,
                color: '#111',
                align: 'center',
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            color:'#02dbd0'
        },
		]
    });

    $('#cart_bar_perempuan').highcharts({
        chart: {
            type: 'column',
            marginLeft:60,
            marginRight:10,
            reflow: true
        },
        title: {
            text: ''
        },
        credits: {
          enabled:false
        },
        exporting:{
          enabled:false
        },
        subtitle: {
            text: '<b>STATISTIK DATA PEREMPUAN PER PROVINSI</b>'
        },
        plotOptions: {
            column : {
                pointWidth:23
            }
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -35,
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.y:f}</b>'
        },
        series: [{
            name: 'Jumlah Data Perempuan',
            data: [<?php echo $perempuan;?>],
            dataLabels: {
                enabled: true,
                color: '#111',
                align: 'center',
                style: {
                    fontSize: '9px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            color:'#02dbd0'
        }]
    });
	
});

</script>
                            