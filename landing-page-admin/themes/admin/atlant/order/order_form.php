<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/save" class="form-horizontal" method="post"> 
        <input type="hidden" name="order_id" id="order_id" value="<?php echo isset($val->order_id)?$val->order_id:'';?>" />
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Input Order Baru</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Tanggal Order</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control digits" type="date" name="order_date" value="<?php echo isset($val)?$val->order_date:date("Y-m-d");?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Nama Customer</label>
                                                <div class="col-sm-9">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="order_customer">
														<option value=""> - Pilih Customer - </option>
														<?php foreach((array)get_customer() as $kj=>$vj){
																$s = isset($val)&&$val->order_customer==$vj->customer_id?'selected="selected"':'';
																echo "<option value='".$vj->customer_id."' $s >".$vj->customer_name."</option>";
														} ?>
													</select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Tanggal Pick Up</label>
                                                <div class="col-sm-9">
                                                    <input type="date" class="form-control digits" placeholder="Tanggal Pick Up Order" name="order_pick_up" value="<?php echo isset($val)?$val->order_pick_up:"";?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Lama Order (hari)</label>
                                                <div class="col-sm-9">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="order_day">
														<option value=""> - Pilih Lama Order (dalam Hari) - </option>
														<?php for($a=1;$a<=20;$a++){
																$s = isset($val)&&$val->order_day==$a?'selected="selected"':'';
																echo "<option value='".$a."' $s >".$a." hari</option>";
														} ?>
													</select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Nomor Container</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan Nomor Container" name="order_container_number" value="<?php echo isset($val)?$val->order_container_number:"";?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Armada / Kendaraan</label>
                                                <div class="col-sm-9">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="order_armada">
														<option value=""> - Pilih Armada / Kendaraan - </option>
														<?php foreach((array)get_armada() as $kj=>$vj){
																$s = isset($val)&&$val->order_armada==$vj->armada_id?'selected="selected"':'';
																echo "<option value='".$vj->armada_id."' $s >".$vj->plat_nomor."</option>";
														} ?>
													</select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Supir / Pengendara</label>
                                                <div class="col-sm-9">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="order_driver">
														<option value=""> - Pilih Supir / Pengendara - </option>
														<?php foreach((array)get_driver() as $kj=>$vj){
																$s = isset($val)&&$val->order_driver==$vj->driver_id?'selected="selected"':'';
																echo "<option value='".$vj->driver_id."' $s >".$vj->driver_name."</option>";
														} ?>
													</select>
                                                </div>
                                            </div>											
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Tujuan Order</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Tujuan Order</label>
                                                <div class="col-sm-9">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="order_destination">
														<option value=""> - Pilih Tujuan Order - </option>
														<?php foreach((array)get_tujuan() as $kj=>$vj){
																$s = isset($val)&&$val->order_destination==$vj->tujuan_id?'selected="selected"':'';
																echo "<option value='".$vj->tujuan_id."' $s >".$vj->tujuan."</option>";
														} ?>
													</select>
                                                </div>
                                            </div>
											<?php if(isset($val)){ ?>
											<?php $no=1; foreach((array)get_order_detail($val->order_id) as $kj=>$vj){ $no++;?>
												<div class="form-group row">
													<label class="col-sm-3 col-form-label">Biaya <?php echo $vj->jo_name;?></label>
														<div class="col-sm-9">
															<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp.</span>
															</div>
															<input type="text" name="biaya-<?php echo $vj->jo_id;?>" class="form-control" placeholder="Masukan Biaya <?php echo $vj->jo_name;?>" value="<?php echo $vj->amount;?>">
														</div>
													</div>
												</div>												
											<?php } ?>
											<?php }else{ ?>
											<?php $no=1; foreach((array)get_jo() as $kj=>$vj){ $no++;?>
												<div class="form-group row">
													<label class="col-sm-3 col-form-label">Biaya <?php echo $vj->jo_name;?></label>
														<div class="col-sm-9">
															<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp.</span>
															</div>
															<input type="text" name="biaya-<?php echo $vj->jo_id;?>" class="form-control" placeholder="Masukan Biaya <?php echo $vj->jo_name;?>" value="0">
														</div>
													</div>
												</div>												
											<?php } ?>
											<?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-info">Proses Order</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>