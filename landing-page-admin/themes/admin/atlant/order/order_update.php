<?php $status = cfg('status-order');?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Order Detail - <?php echo get_nama_cust($val->order_customer)->customer_name;?> / <?php echo $val->order_code;?></h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
										<div class="table-responsive">
											<table class="table">
												<tbody>
												<tr>
													<td widtd="150px">Tgl. Order</td>
													<td widtd="2px">:</td>
													<td align="left"><?php echo myDate($val->order_date,"d M Y");?></td>
													<td>Customer</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_cust($val->order_customer)->customer_name;?></td>
												</tr>
												<tr>
													<td widtd="150px">Tgl. Pick Up</td>
													<td widtd="2px">:</td>
													<td align="left"><?php echo myDate($val->order_pick_up,"d M Y");?></td>
													<td>Tujuan</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_tujuan($val->order_destination)->tujuan;?></td>
												</tr>
												<tr>
													<td>Armada / Supir</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_armada($val->order_armada)->plat_nomor;?> /  <?php echo get_nama_driver($val->order_driver)->driver_name;?></td>
													<td>Biaya</td>
													<td>:</td>
													<td align="left">Rp. <?php echo myNum($val->order_amount);?></td>
												</tr>
												<tr>
													<td>Status Order</td>
													<td>:</td>
													<td align="left"><?php echo $status[$val->order_status];?></td>
													<td>No. Container</td>
													<td>:</td>
													<td align="left"><?php echo $val->order_container_number;?></td>
												</tr>
												</tbody>
											</table>
										</div>									
                                    </div>
                                </div>
                        </div>
                    </div>
                    </div>
					
					
                <div class="row">
					<?php if($val->order_status == 2){ ?>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                <h5>PICK UP</h5>
                            </div>
							<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/update_pickup" class="form-horizontal" metdod="post">
								<input type="hidden" name="order_id" id="order_id" value="<?php echo isset($val->order_id)?$val->order_id:'';?>" />							
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Armada</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" value="<?php echo isset($val)?get_nama_armada($val->order_armada)->plat_nomor:"";?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Driver</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" value="<?php echo isset($val)?get_nama_driver($val->order_driver)->driver_name:"";?>">
                                                </div>
                                            </div>										
                                        </div>									
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-info">Proses Pick Up</button>
                                    </div>
                                </div>
							</form>
                        </div>
						</div>
					<?php }elseif($val->order_status == 3){ ?>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                <h5>CLOSE ORDER</h5>
                            </div>
							<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/update_close" class="form-horizontal" method="post">
								<input type="hidden" name="order_id" id="order_id" value="<?php echo isset($val->order_id)?$val->order_id:'';?>" />
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tanggal Kembali</label>
                                                <div class="col-sm-8">
                                                    <input type="date" class="form-control digits" placeholder="Tanggal Pick Up Order" name="order_done" value="">
                                                </div>
                                            </div>
                                        </div>									
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-info">Submit Close Order</button>
                                    </div>
                                </div>		
							</form>
                        </div>
						</div>
					<?php }?>
						
					<?php if($val->order_status != 4){ ?>
                    <div class="col-sm-8">
                        <div class="card">
                            <div class="card-header">
                                <h5>INPUT BIAYA ORDER</h5>
                            </div>
							<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/update_biaya" class="form-horizontal" method="post">
								<input type="hidden" name="order_id" id="order_id" value="<?php echo isset($val->order_id)?$val->order_id:'';?>" />
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Tgl. Transaksi</label>
                                                <div class="col-sm-9">
                                                    <input type="date" class="form-control digits" placeholder="Tanggal Pick Up Order" name="trx_date">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Deskripsi</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Input Deskripsi Transaksi" name="trx_desc">
                                                </div>
                                            </div>											
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Kas</label>
                                                <div class="col-sm-4">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="kas">
														<option value=""> - Pilih Kas - </option>
														<?php foreach((array)get_spec_akun(1) as $kj=>$vj){
																echo "<option value='".$vj->coa_code."' $s >".$vj->coa_name."</option>";
														} ?>
													</select>
                                                </div>
                                                <label class="col-sm-1 col-form-label">Akun</label>
                                                <div class="col-sm-4">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="akun">
														<option value=""> - Pilih Akun - </option>
														<?php foreach((array)get_spec_akun(6) as $kj=>$vj){
																echo "<option value='".$vj->coa_code."' $s >".$vj->coa_name."</option>";
														} ?>
													</select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Biaya</label>
                                                <div class="col-sm-6">
															<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text">Rp.</span>
															</div>
															<input type="text" name="biaya" class="form-control" placeholder="Masukan Biaya" value="0">
														</div>
                                                </div>
                                            </div>	
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"></label>
                                                <div class="col-sm-9">
															<button type="submit" class="btn btn-info">Tambah Biaya</button>
                                                </div>
                                            </div>											
                                        </div>									
                                    </div>
                                </div>
							</form>
                        </div>
                    </div>
					<?php } ?>
                    </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Order Detail - <?php echo get_nama_cust($val->order_customer)->customer_name;?> / <?php echo $val->order_code;?></h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
										<div class="table-responsive">
											<table class="table table-striped">
													<tr>
														<td align="left"><b>No.</b></td>
														<td align="left"><b>Tgl. Transaksi</b></td>
														<td align="left"><b>Akun</b></td>
														<td align="left"><b>Deskripsi Transaksi</b></td>
														<td align="left"><b>Debit</b></td>
														<td align="left"><b>Kredit</b></td>
													</tr>
												<tbody>
												<?php $no=0; $total_debit=0; $total_kredit=0;
													  foreach((array)get_order_trx($val->order_id) as $kj=>$vj){ $no++;
													  $total_debit = $total_debit + $vj->jurnal_debit;
													  $total_kredit = $total_kredit + $vj->jurnal_kredit;
												?>
													<tr>
														<td align="left"><?php echo $no;?></td>
														<td align="left"><?php echo $vj->jurnal_date;?></td>
														<td align="left"><?php echo $vj->coa_name;?></td>
														<td align="left"><?php echo $vj->jurnal_desc;?></td>
														<td align="left">Rp. <?php echo myNum($vj->jurnal_debit);?></td>
														<td align="left">Rp. <?php echo myNum($vj->jurnal_kredit);?></td>
													</tr>
												<?php } ?>												
												</tbody>
													<tr>
														<td align="left"></td>
														<td align="left"></td>
														<td align="left"></td>
														<td align="left"><b>TOTAL</b></td>
														<td align="left"><b>Rp. <?php echo myNum($total_debit);?></b></td>
														<td align="left"><b>Rp. <?php echo myNum($total_kredit);?></b></td>
													</tr>
											</table>
										</div>									
                                    </div>
                                </div>
                        </div>
                    </div>
                    </div>					
				</div>				