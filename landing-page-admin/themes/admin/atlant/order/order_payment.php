                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Update Status Pembayaran Order</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
										<div class="table-responsive">
											<table class="table">
												<thead>
												<tbody>
												<tr>
													<td width="150px">Tgl. Order</td>
													<td width="2px">:</td>
													<td align="left"><?php echo myDate($val->order_date,"d M Y");?></td>
												</tr>
												<tr>
													<td>Customer</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_cust($val->order_customer)->customer_name;?></td>
												</tr>
												<tr>
													<td width="150px">Tgl. Pick Up</td>
													<td width="2px">:</td>
													<td align="left"><?php echo myDate($val->order_pick_up,"d M Y");?></td>
												</tr>
												<tr>
													<td>Armada / Supir</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_armada($val->order_armada)->plat_nomor;?> /  <?php echo get_nama_driver($val->order_driver)->driver_name;?></td>
												</tr>
												<tr>
													<td>Tujuan</td>
													<td>:</td>
													<td align="left"><?php echo get_nama_tujuan($val->order_destination)->tujuan;?></td>
												</tr>
												<tr>
													<td>No. Container</td>
													<td>:</td>
													<td align="left"><?php echo $val->order_container_number;?></td>
												</tr>
												<tr>
													<td>Status Order</td>
													<td>:</td>
													<td align="left"><?php echo $val->order_container_number;?></td>
												</tr>
												</tbody>
											</table>
										</div>									
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Biaya Order</h5>
                            </div>
                                <div class="card-body">
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
												<tbody>
												<?php $no=1; $total=0; foreach((array)get_order_detail($val->order_id) as $kj=>$vj){ $no++;
													  $total = $total + $vj->amount;
												?>
													<tr>
														<td width="300px" align="left"><?php echo $vj->jo_name;?> <?php echo $val->order_day;?> hari</td>
														<td width="2px">:</td>
														<td align="left">Rp. <?php echo myNum($vj->amount);?></td>
													</tr>
												<?php } ?>
													<tr>
														<td width="300px" align="left">Total Biaya</td>
														<td width="2px">:</td>
														<td align="left"><strong>Rp. <?php echo myNum($total);?></strong></td>
													</tr>
												</tbody>
											</table>
										</div>											
                                    </div>	
                                <div class="card-footer">
									<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/update_payment" class="form-horizontal" method="post"> 
											<input type="hidden" name="order_id" id="order_id" value="<?php echo isset($val->order_id)?$val->order_id:'';?>" />								
											<input type="hidden" name="order_amount" id="order_amount" value="<?php echo isset($val->order_amount)?$val->order_amount:'';?>" />								
											<input type="hidden" name="order_code" id="order_code" value="<?php echo isset($val->order_code)?$val->order_code:'';?>" />								
											<input type="hidden" name="order_custmer" id="order_customer" value="<?php echo isset($val->order_customer)?get_nama_cust($val->order_customer)->customer_name:'';?>" />								
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Akun Pemasukan</label>
                                                <div class="col-sm-9">
													<select class="form-control form-control digits" id="exampleFormControlSelect29" name="akun">
														<option value=""> - Pilih Akun - </option>
														<?php foreach((array)get_spec_akun(1) as $kj=>$vj){
																echo "<option value='".$vj->coa_code."' $s >".$vj->coa_name."</option>";
														} ?>
													</select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Tanggal Terima Pembayaran</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control digits" type="date" name="payment_date" value="<?php echo date("Y-m-d");?>">
                                                </div>
                                            </div>
											<div class="col-sm-9 offset-sm-3">
													<button type="submit" class="btn btn-info">Proses Pembayaran</button>
											</div>
									</form>
                                </div>									
                                </div>
                        </div>
                    </div>
				</div>				