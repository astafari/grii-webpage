<style type="text/css">
.border_camera{
    width:170px;height:130px;
    border:5px solid #FE4A3F;
    margin: 10px;
}
.content {
	background-image: url('<?php echo base_url()."assets/images/id_card_front.jpg";?>'); 
	background-size: 350px 220px;
	background-repeat: no-repeat; 
	height:220px; 
	width:350px;
}
.nama,.nomor,.domisili,.masa{
	text-align: right;
}
.nama{
	font-size: 16px;
	font-weight: bold;
	line-height: 20px;
}
.nomor{
	font-size: 12px;
	line-height: 12px;
}
.domisili{
	font-size: 10px;
	line-height: 15px;
}
.masa{
	font-size: 7px;
	margin-top: 5px;
}

.canvas {
	height:650px; 
	width:1063px;
}
.content-lg { 
	background-image: url('<?php echo base_url()."assets/images/id_card_front.jpg";?>'); 
	background-size: 100% 100%;
	background-repeat: no-repeat;
	height:650px; 
	width:1063px;
}
.content-lg-no-image { 
	background-color:transparent; 
	/*background-size: 100% 100%;
	background-repeat: no-repeat;*/
	height:650px; 
	width:1063px;
}
.nama-lg,.nomor-lg,.domisili-lg,.masa-lg{
	text-align: right;
}
.nama-lg{
	font-size: 50px;
	font-weight: bold;
	line-height: 50px;
}
.nomor-lg{
	font-size: 35px;
	line-height: 50px;
}
.domisili-lg{
	font-size: 30px;
	line-height: 28px;
}
.masa-lg{
	font-size: 18px;
	margin-top: 30px;
}
</style>
<?php js_validate();
$status = cfg('status_anggota');
$asuransi = cfg('status_asuransi');
$tipe_kta = cfg('tipe_kta');
$jenkel = cfg('jenkel');
$jenis_bayar = cfg('jenis_bayar');
$status_nikah = cfg('status_nikah');
$pendidikan = cfg('pendidikan');
$pekerjaan = cfg('pekerjaan');
$tingkat = cfg('tingkatan');
$jabatan = cfg('jabatan');
$hastakarya = cfg('hasta_karya');
?>
<div class="panel panel-default tabs">                            
	<ul class="nav nav-tabs" role="tablist">
	    <li><a href="<?php echo site_url()?>" role="tab">Dashboard Maps Distribution</a></li>
	    <li><a href="<?php echo site_url('meme/me/detail_kta')?>" role="tab">Statistik Anggota</a></li>
	    <li><a href="<?php echo site_url('meme/me/detail_kta')?>" role="tab">Grafik Data Anggota</a></li>
	    <li  class="active"><a href="<?php echo site_url('meme/me/verifikasi')?>" role="tab">Cari Data Anggota</a></li>
	</ul>                            
	<div class="panel-body tab-content">
	    <div class="tab-pane active">
				<form id="form-show" action="<?php echo $own_links;?>/verifikasi" class="form-horizontal" method="post" autocomplete="off" > 
					<div class="row">         
	          			<div class="col-md-12">   
							<div class="row">       
								<div class="col-md-3 control-label">Masukan NPAPG / NIK / Nama Anggota</div>
								<div class="col-md-5">
									<input type="text" name="verifikasi" id="verifikasi"  class="form-control" placeholder="Verifikasi Anggota"  
									onchange="getVerificationMember($(this).val())"/>
								</div>
							</div>
							<br />
							<div class="panel-body panel-body-table" style="height: 150px; overflow-y: scroll;">			
								<div class="table-responsive">
									<table class="table table-hover table-bordered" id="dash_two">
									   <thead>
										<tr>
											<th>NPAPG</th>
											<th>NIK</th>
											<th>Nama Lengkap</th>
											<th>Domisili</th>
											<th>Action</th>
										</tr>
										</thead>
									    <tbody>
										</tbody>
									</table>
								</div>
							</div>
			            </div>
					</div>
					<?php if($m != 'zero'){ $id = empty($m)?"":_decrypt($m);  ?>
					<?php foreach ((array)get_detail_anggota($id) as $p => $m) { ?>					
			        <div class="panel-body">                                                                        			            
			            <div class="row">
			                <div class="col-md-6">
			                    <h3>Data Anggota</h3>
			                    <table class="table table-striped">
			                        <tbody>

										<tr>
                                <td colspan="3"><hr></td>
                            </tr>
                            <tr>
                                <td>Tipe KTA</td>
                                <td width="1">:</td>
                                <td><?php echo $m->kta_tipe_kta;?></td>
                            </tr>
                            <tr>
                                <td>NPAPG</td>
                                <td width="1">:</td>
                                <td><?php echo substr($m->kta_nomor_kartu,0,6)." ".substr($m->kta_nomor_kartu,6,6)." ".substr($m->kta_nomor_kartu,12,4);?></td>
                            </tr>
                            <tr>
                                <td>Nama Lengkap</td>
                                <td width="1">:</td>
                                <td><?php echo $m->kta_nama_lengkap;?></td>
                            </tr>
                            <tr>
                                <td>No. KTP / NIK</td>
                                <td width="1">:</td>
                                <td><?php echo $m->kta_no_id;?></td>
                            </tr>
                        <tr>
                                <td colspan="3"><hr></td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td width="1">:</td>
                                <td><?php if(empty($m->kta_jenkel)){ echo "-"; }else{ echo $jenkel[$m->kta_jenkel]; }?></td>
                            </tr>
                            <tr>
                                <td>Tempat / Tanggal lahir</td>
                                <td width="1">:</td>
                                <td><?php echo $m->kta_tempat_lahir;?> / <?php echo myDate($m->kta_tgl_lahir,"d M Y");?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td width="1">:</td>
                                <td><?php echo $m->kta_alamat;?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td width="1"></td>
                                <td><?php 
								$rt = isset($m->kta_rt)?$m->kta_rt:'-';
								$rw = isset($m->kta_rw)?$m->kta_rw:'-';
								$kp = isset($m->kta_kodepos)?$m->kta_kodepos:'-';								
								echo "RT/RW : ".$rt." / ".$rw." KODE POS : ".$kp;?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td width="1"></td>
                                <td>
									<?php 
									foreach ((array)get_domisili_kta($m->kta_id) as $k => $v) {
										echo $v->kel_nama." , ".$v->kec_nama." , ".$v->kab_nama." - ".$v->propinsi_nama;
									}
									?>														
								</td>
                            </tr>
                            <tr>
                                <td>NPAPG Lama</td>
                                <td width="1">:</td>
                                <td><?php echo empty($m->kta_nomor_kartu_old)?"-":$m->kta_nomor_kartu_old;?></td>
                            </tr>
							<?php
								foreach ((array)get_dpp_kta($m->kta_id) as $k => $v) {
							?>
                            <tr>
                                <td>Nama DPD Propinsi</td>
                                <td width="1">:</td>
                                <td><?php echo empty($v->propinsi_nama)?"-":$v->propinsi_nama;?></td>
                            </tr>
                            <tr>
                                <td>Nama DPD Kab / Kota</td>
                                <td width="1">:</td>
                                <td><?php echo empty($v->kab_nama)?"-":$v->kab_nama;;?></td>
                            </tr>
                            <tr>
                                <td>Nama PK Kecamatan</td>
                                <td width="1">:</td>
                                <td><?php echo empty($v->kec_nama)?"-":$v->kec_nama;;?></td>
                            </tr>
                            <tr>
                                <td>Nama PL Kel / Desa</td>
                                <td width="1">:</td>
                                <td><?php echo empty($v->kel_nama)?"-":$v->kel_nama;;?></td>
                            </tr>
                            <tr>
                                <td>Keanggotaan Hasta Karya</td>
                                <td width="1">:</td>
                                <td><?php echo $m->kta_trikarya.", ".$m->kta_sayap.", ".$m->kta_hastakarya;?></td>
                            </tr>
							<?php
								}
							?>
			                        </tbody>
			                    </table>
			                  
			                </div>
			                <div class="col-md-6">
			                    <h3>Data Tambahan</h3>
			                        <table class="table table-striped">
			                        <tbody>
			                            <tr>
			                                <td colspan="3" align="center">
			                                	<div class="content">
													<div style="clear:both;float:right;margin:8px 9px;">
														<?php $qrcode = generate_qr_code(substr($m->kta_nomor_kartu,0,6).substr($m->kta_nomor_kartu,6,4).substr($m->kta_nomor_kartu,10,6));?>
														<img src="<?php echo $qrcode;?>" style="height:38px; width:38px;" />
													</div>
													
													<div style="clear:both;float:right;margin:3px 21px 0 0;">
														<img alt="" src="<?php echo empty($val->kta_foto_wajah)?base_url().'assets/images/no_photo.jpg':base_url()."assets/collections/kta/photo/".$val->kta_foto_wajah;?>" style="height:97px; width:74px;" >
														<!-- img alt="" src="</?php echo empty($m->kta_foto_wajah)?base_url().'assets/images/no_image.jpg':base_url()."assets/collections/kta/photo/".$m->kta_foto_wajah;?>" style="height:97px; width:74px;" -->
													</div>
											
													<div style="clear:both;float:right;margin-right:22px;">
														<div class="nama"><?php echo strtoupper($m->kta_nama_lengkap);?></div>
														<div class="nomor">NPAPG <?php echo substr($m->kta_nomor_kartu,0,6)." ".substr($m->kta_nomor_kartu,6,6)." ".substr($m->kta_nomor_kartu,12,4);?></div>
														<div class="domisili"><?php echo $m->kab_nama." - ".$m->propinsi_nama;?></div>
														<div class="masa"><?php echo date('m/Y',strtotime($m->time_add));?></div>
													</div>
												</div><br/>
												<!-- 
												<img alt="" src="<?php echo get_image(base_url()."assets/collections/kta/original/".$m->kta_kartu);?>" width="350" align = "center">
												<img alt="" src="</?php echo get_image(base_url()."assets/images/id_card_front.jpg");?>" width="350" align="center">
												 -->			
											</td>
			                            </tr>
			                            <tr>
			                                <td colspan="3"><hr></td>
			                            </tr>
										<tr>
											<td>Status KTA</td>
											<td width="1">:</td>
											<td width="400">
											<?php if($m->kta_status_data == 0 ){
													echo '<span class="label label-warning label-form" m-toggle="tooltip" m-placement="top" title m-original-title="Waiting to Approve"><li class="fa fa fa-spinner"></li> Pending</span>';								
												  }elseif($m->kta_status_data == 1 ){
													echo '<span class="label label-success label-form" m-toggle="tooltip" m-placement="top" title m-original-title="Approved"><li class="fa fa-check-circle"></li> Approved</span>';								
												  }elseif($m->kta_status_data == 2 ){
													echo '<span class="label label-info label-form" m-toggle="tooltip" m-placement="top" title m-original-title="Waiting to Entry"><li class="fa fa fa-spinner"></li> Uploaded</span>';								
												  }elseif($m->kta_status_data == 3){
													echo '<span class="label label-danger label-form" m-toggle="tooltip" m-placement="top" title m-original-title="Rejected by Operator Entry"><li class="glyphicon glyphicon-ban-circle"></li> Rejected</span>';								
												  }elseif($m->kta_status_data == 4){
													echo '<span class="label label-danger label-form" m-toggle="tooltip" m-placement="top" title m-original-title="Rejected by Koordinator Data"><li class="glyphicon glyphicon-ban-circle"></li> Rejected</span>';								
												  }else{
													echo '<span class="label label-danger label-form" m-toggle="tooltip" m-placement="top" title m-original-title="Rejected"><li class="glyphicon glyphicon-ban-circle"></li></span>';								
												  }
											?>
											<?php // link_action($links_table_item,"?_id="._encrypt($r->kta_id));?>
											</td>
										</tr>
			                            <tr>
			                                <td>Tanggal Approve</td>
			                                <td width="1">:</td>
			                                <td><?php echo myDate($m->time_approve,"d M Y H:i:s");?></td>
			                            </tr>
			                            <tr>
			                                <td>Tanggal Reject</td>
			                                <td width="1">:</td>
			                                <td><?php echo myDate($m->time_reject_entry,"d M Y H:i:s");?></td>
			                            </tr>
			                            <tr>
			                                <td>Keterangan Reject</td>
			                                <td width="1">:</td>
			                                <td><?php echo $m->col6;?></td>
			                            </tr>
			                            <tr>
			                                <td>Status Kartu</td>
			                                <td width="1">:</td>
			                                <td>
												<?php if($m->is_cetak == 0 ){
														echo '<span class="label label-warning" data-toggle="tooltip" data-placement="top" title data-original-title="Belum Tercetak"><li class="fa fa fa-spinner"></li> Belum Cetak</span>';								
													  }elseif($m->is_cetak == 1 ){
														echo '<span class="label label-success" data-toggle="tooltip" data-placement="top" title data-original-title="Sudah Tercetak"><li class="fa fa-download"></li> Sudah Cetak</span>';								
													  }elseif($m->is_cetak == 2 ){
														echo '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Dikirim ke Pabrik Untuk Dicetak"><li class="fa fa fa-spinner"></li> Siap Cetak</span>';								
													  }else{
														echo '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Proses Pencetakan"><li class="glyphicon glyphicon-ban-circle"></li> Proses Pencetakan</span>';								
													  }
												?>										
											</td>
			                            </tr>
			                            <tr>
			                                <td>Tanggal Cetak Kartu</td>
			                                <td width="1">:</td>
			                                <td><?php echo $m->time_print_card;?></td>
			                            </tr>
			                            <tr>
			                                <td>Petugas Print</td>
			                                <td width="1">:</td>
			                                <td><?php echo $m->col14;?></td>
			                            </tr>

			                        </tbody>
			                    </table>
			                </div>			                
			            </div>			
			        </div>
					<div class="row">                                
						<div class="form-group">
							<div class="col-md-12">
							<a href="<?php echo $own_links."/delete_anggota/?_id="._encrypt($m->kta_id);?>" class='tip act_confirm' rel='600|150' data-title='Konfirmasi Hapus Data' data-icon='fa-trash-o' data-desc='Pastikan data yang anda akan hapus ini benar, agar tidak terjadi kesalahan.' data-body='Apakah anda yakin data akan dihapus ??'"><span class="btn btn-danger btn-lg" data-toggle="tooltip" data-placement="top" title data-original-title="Hapus Data Ini" ><li class="fa fa-times"></li> Hapus Data Anggota Ini</span></a>		
							<?php if($m->is_cetak != 0){ ?>
							<a href="<?php echo $own_links."/cetak_ulang/?_id="._encrypt($m->kta_id);?>" class='tip act_confirm' rel='600|150' data-title='Konfirmasi Cetak Ulang' data-icon='fa-trash-o' data-desc='Membuka ulang izin pencetakan data, pencetakan data sebelumnya akan tersimpan didalam tracking data.' data-body='Apakah anda yakin akan membuka flag cetak?'"><span class="btn btn-success btn-lg" data-toggle="tooltip" data-placement="top" title data-original-title="Cetak Ulang Kartu" ><li class="fa fa-times"></li> Cetak Ulang Kartu</span></a>		
							<?php } ?>
							</div>
						</div>
					</div>					
					<?php }?>
					<?php }?>
				</form>
<script type="text/javascript">
var AJAX_URL = '<?php echo site_url("ajax/data");?>';
$(document).ready(function(){
	$("#verifikasi").keypress(function(e){
		if(e.which == 13){
 		  e.preventDefault();
 		  getVerificationMember($(this).val());
		}
	});
	$("#verifikasi").bind("enterKey",function(e){
			e.preventDefault();
			getVerificationMember($(this).val());
//			return false();
	});
});

function getVerificationMember(verifikasi) {
    $('#dash_two tbody').html('<tr><td colspan="5" style="text-align: center">Mohon Tunggu...</td></tr>');
	$.post(AJAX_URL+"/get_verification_data",{verifikasi:verifikasi},function(o){
			$('#dash_two tbody').html(o);
	});		
}
</script>			