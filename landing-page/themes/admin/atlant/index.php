  <div class="clearfix"></div>
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="demo-full-width">
          <div id="grid-container" class="cbp">
            <div class="cbp-item identity logos"> <a href="<?php echo themeUrl();?>images/image_1.jpg" target="_blank" class="cbp-caption cbp-lightbox" data-title="KU Mand Livestream Minggu 7:30 WIB">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_1.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">KU Mand Livestream Minggu 7:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item web-design"> <a href="<?php echo themeUrl();?>images/image_4.jpg" class="cbp-caption cbp-lightbox" data-title="Magna Tempus Urna<br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_4.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">PD Pagi Livestream Sabtu 6:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item motion identity"> <a href="<?php echo themeUrl();?>images/image_2.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_2.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">KU Indo Livestream  Minggu 9:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item identity graphic"> <a href="<?php echo themeUrl();?>images/image_3.jpg" class="cbp-caption cbp-lightbox" data-title="Quisque Ornare <br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_3.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">English Worship Service Sun 10:30 WIB </div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="clearfix"></div>
  
  <section>
    <div class="pagenation-holder">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3>Our Upcoming Events</h3>
          </div>
          <div class="col-md-6 text-right">
            <div class="pagenation_links"><a href="index.php">Home</a><i> / </i> Upcoming Events</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>
  
  <section class="sec-padding">
  <!--end right column-->     
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="calendar calendar-first" id="calendar_first">
            <div class="calendar_header">
                <button class="switch-month switch-left"> <i class="fa fa-chevron-left"></i></button>
                 <h2></h2>
                <button class="switch-month switch-right"> <i class="fa fa-chevron-right"></i></button>
            </div>
            <div class="calendar_weekdays"></div>
            <div class="calendar_content"></div>
        </div>
        <!--end item-->          
        <div class="clearfix"></div>
      </div>
        <div class="col-md-8">
          <div class="col-xs-12 text-left">
            <div class="clearfix"></div>
            <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <div class="main-timeline">
                        <?php
                          foreach ((array)get_event() as $p => $q) {
                        ?>
                          <div class="timeline">
                              <div class="timeline-content">
                                  <div class="timeline-icon">
                                      <i class="fa fa-calendar"></i>
                                  </div>
                                  <h3 class="title"><?php echo myDate($q->date,"d M Y");?></h3>
                                  <p class="description">
                                      <a href="#"><?php echo $q->judul;?></a><br><br>
                                      <a href="<?php echo $q->link_1;?>" target="_blank" class="col-form-label-sm"><i class="fa fa-youtube-play"></i> Link 1</a> <br>
                                      <a href="<?php echo $q->link_2;?>" target="_blank" class="col-form-label-sm"><i class="fa fa-youtube-play"></i> Link 2</a><br>
                                      <a href="http://reformed21.tv" target="_blank" class="col-form-label-sm"><img src="<?php echo themeUrl();?>images/icon/electronics.png"> Reformed 21 TV</a> <br>
                                      <a href="#" class="col-form-label-sm"><img src="<?php echo themeUrl();?>images/icon/electronics.png"> Zoom Link</a> 
                                  </p>
                              </div>
                          </div>
                        <?php
                          }
                        ?>
                  </div>
                      </div>
                  </div>
              </div>
          </div>            
          </div>
          <div class="clearfix"></div>
          
        </div>
        <!--end left column-->

        
      </div>
    </div>
  </section>
  <!-- end section -->
  <div class="clearfix"></div>
