<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/save" class="form-horizontal" method="post"> 
        <input type="hidden" name="armada_id" id="armada_id" value="<?php echo isset($val->armada_id)?$val->armada_id:'';?>" />
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Input Data Armada</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Jenis Kendaraan</label>
                                                <div class="col-sm-9">
                                                <select class="form-control form-control digits" id="exampleFormControlSelect29" name="jm">
													<?php foreach ((array)get_jenis_kendaraan() as $m) {
														$s = isset($val)?($val->jm_id==$m->jm_id?'selected="selected"':''):'';
														echo "<option value='".$m->jm_id."' $s >".$m->jm_name."</option>";
													}?>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Plat Nomor</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan Plat Nomor Kendaraan" name="plat" value="<?php echo isset($val)?$val->plat_nomor:"";?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Kondisi Kendaraan</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Keterangan</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" id="exampleFormControlTextarea4" rows="3" name="keterangan"><?php echo isset($val)?$val->keterangan:"";?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Status Kendaraan</label>
                                                <div class="col-sm-9">
                                                <select class="form-control form-control digits" id="exampleFormControlSelect29" name="status">
                                                    <?php foreach((array)cfg('status-armada') as $kj=>$vj){
															$s = isset($val)&&$val->status==$kj?'selected="selected"':'';
															echo "<option value='".$kj."' $s >".$vj."</option>";
													} ?>
                                                </select>
                                                </div>
                                            </div>												
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>