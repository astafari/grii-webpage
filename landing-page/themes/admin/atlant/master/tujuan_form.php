<form id="form-validated" enctype="multipart/form-data" action="<?php echo $own_links;?>/save" class="form-horizontal" method="post"> 
        <input type="hidden" name="tujuan_id" id="tujuan_id" value="<?php echo isset($val->tujuan_id)?$val->tujuan_id:'';?>" />
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Input Data Tujuan</h5>
                            </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Tujuan</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" placeholder="Masukan Tujuan " name="tujuan" value="<?php echo isset($val)?$val->tujuan:"";?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Status</label>
                                                <div class="col-sm-9">
                                                <select class="form-control form-control digits" id="exampleFormControlSelect29" name="status">
                                                    <?php foreach((array)cfg('status') as $kj=>$vj){
															$s = isset($val)&&$val->status==$kj?'selected="selected"':'';
															echo "<option value='".$kj."' $s >".$vj."</option>";
													} ?>
                                                </select>
                                                </div>
                                            </div>												
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>								
                        </div>
                    </div>
				</div>