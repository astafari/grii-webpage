<!doctype html>
<html lang="en">
<head>
<title>GRII - Gereja Reformed Injili Indonesia</title>
<meta charset="utf-8">
<!-- Meta -->
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="robots" content="" />
<meta name="description" content="" />

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Stylesheets -->
<link rel="stylesheet" media="screen" href="<?php echo themeUrl();?>js/bootstrap/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>js/mainmenu/menu.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/default.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/layouts.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/shortcodes.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/timeline.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/calendar.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" media="screen" href="<?php echo themeUrl();?>css/responsive-leyouts.css" type="text/css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>js/masterslider/style/masterslider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>js/cubeportfolio/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/et-line-font/et-line-font.css">
<link href="<?php echo themeUrl();?>js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo themeUrl();?>js/jFlickrFeed/style.css" />
<link rel="stylesheet" href="<?php echo themeUrl();?>css/colors/brown.css" />
</head>
<body>
<div class="site_wrapper">  
  <div id="header">
    <div class="container">
      <div class="navbar brown navbar-default yamm">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle two three"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          <a href="index.php" class="navbar-brand less-top-padding"><img src="<?php echo themeUrl();?>images/logo.png" alt=""/></a> </div>
            <div id="navbar-collapse-grid" class="navbar-collapse collapse pull-right">
              <ul class="nav brown navbar-nav">
                <li><a href="index.php" class="dropdown-toggle">Event</a></li>
                <li><a href="<?php echo site_url('meme/me/past_event');?>" class="dropdown-toggle">Past Event</a></li>
              </ul>
            </div>
        </div>
      </div>
  </div>
  <!--end menu-->