<div class="clearfix"></div>
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="demo-full-width">
          <div id="grid-container" class="cbp">
            <div class="cbp-item identity logos"> <a href="<?php echo themeUrl();?>images/image_1.jpg" target="_blank" class="cbp-caption cbp-lightbox" data-title="KU Mand Livestream Minggu 7:30 WIB">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_1.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">KU Mand Livestream Minggu 7:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item web-design"> <a href="<?php echo themeUrl();?>images/image_4.jpg" class="cbp-caption cbp-lightbox" data-title="Magna Tempus Urna<br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_4.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">PD Pagi Livestream Sabtu 6:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item motion identity"> <a href="<?php echo themeUrl();?>images/image_2.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_2.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">KU Indo Livestream  Minggu 9:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item identity graphic"> <a href="<?php echo themeUrl();?>images/image_3.jpg" class="cbp-caption cbp-lightbox" data-title="Quisque Ornare <br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="<?php echo themeUrl();?>images/image_3.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">English Worship Service Sun 10:30 WIB </div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="clearfix"></div>
  
  <section>
    <div class="pagenation-holder">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3>Past Events</h3>
          </div>
          <div class="col-md-6 text-right">
            <div class="pagenation_links"><a href="index.html">Home</a><i> / </i> Past Events</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>
  
  <section class="sec-padding">
  <!--end right column-->     
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <h4 class="uppercase oswald">Search Event</h4>
          <form method="post" action="php/smartprocess.php" id="smart-form">
          <div class="form-group">
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Filter by Pendeta">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Filter by Tema / Seri">
                </div>
                <button type="submit" class="btn btn-primary">Search Event</button>
          </form>
        <!--end item-->          
        <div class="clearfix"></div>
        </div>
    
        <div class="col-md-9">
          <div class="col-xs-12">
            <h2 class="uppercase oswald">Past Event List</h2>
            <div class="title-line-4 less-margin"></div>
            <div class="clearfix"></div>
            <div class="container">
              <div class="row">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Date Time</th>
                          <th scope="col">Pendeta</th>
                          <th scope="col">Tema</th>
                          <th scope="col">Link</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td align="center">26 Maret 2020 18:30</td>
                          <td align="center">Pdt. Aiter</td>
                          <td><strong>Plagues of Egypt</strong><br> 
                              Ten Plagues of Egypt - 1 Air Menjadi Darah<br>
                              GRII Citra Raya<br>
                              <code>Mulai Menit Ke-31</code></td>
                              <td align="center">
                                <a href="http://reformed21.tv" target="_blank" class="col-form-label-sm" data-toggle="tooltip" title="Reformed TV"><img src="images/icon/social-media.png"></a> |
                                <a href="http://youtube.com/RELAYRMCI/live" target="_blank" class="col-form-label-sm"><i class="fa fa-youtube" data-toggle="tooltip" title="Youtube"></i> </a> | 
                                <a href="https://livestream.com/STEMI/StreamingReformedInjili" target="_blank" class="col-form-label-sm"><img src="images/icon/electronics.png" data-toggle="tooltip" title="Live Streaming"> 
                              </a>
                        </tr>                        
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>            
          </div>
          <div class="clearfix"></div>          
        </div>
        <!--end left column-->
      </div>
    </div>
  </section>
  <!-- end section -->
  <div class="clearfix"></div>