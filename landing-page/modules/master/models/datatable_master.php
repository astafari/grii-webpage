<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Datatable_master extends CI_Model {
 
    var $table = 'app_kta';
    var $column_order = array(null, 'kta_nkk','kta_nik','kta_nama_lengkap','kel_nama'); //set column field database for datatable orderable
    var $column_search = array('kta_nkk','kta_nik','kta_nama_lengkap','kel_nama'); //set column field database for datatable searchable 
    var $order = array('kta_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
    }
 
    private function _get_datatables_query()
    {
         
        $this->db->select('app_kta.*,kel_nama');
        $this->db->from($this->table);
		$this->db->join('app_kelurahan','app_kelurahan.kel_kode_kpu=app_kta.kta_kelurahan','LEFT');
		/*
		$this->db->join('app_kecamatan','app_kecamatan.kec_kode=app_kelurahan.kel_kec_id','LEFT');
		$this->db->join('app_kabupaten','app_kabupaten.kab_kode=app_kelurahan.kel_kab_id','LEFT');
		$this->db->join('app_propinsi','app_propinsi.propinsi_kode=app_kelurahan.kel_prop_id','LEFT');
		*/
        $i = 0;
		
			$str_like = "( ";
			$i=0;
			foreach ($this->column_search as $item){ // loop column 
					$str_like .= $i!=0?"OR":"";
					$str_like .=" ".$item." LIKE '%".$_POST['search']['value']."%' ";			
					$i++;
			}
			$str_like .= " ) ";
			$this->db->where($str_like);     
//			$this->db->limit(100);
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}