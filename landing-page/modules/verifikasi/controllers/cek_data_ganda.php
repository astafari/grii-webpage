<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Cek_data_ganda extends AdminController {  
	function __construct()    
	{
		parent::__construct(); 
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM"); //"view"
		$this->_set_title('Cek Data Anggota Ganda');
		$this->DATA->table="app_kta";
		$this->folder_view = "verifikasi/";
		$this->prefix_view = strtolower($this->_getClass());

		$this->breadcrumb[] = array(
				"title"		=> "Minima Verifikasi",
				"url"		=> $this->own_link
			);

		$this->is_search_date = false;

		if(!isset($this->jCfg['search']['class']) || $this->jCfg['search']['class'] != $this->_getClass()){
			$this->_reset();
			redirect($this->own_link);
		}
		
	
		$this->cat_search = array(
			''						=> 'All',
			'kab_nama'				=> 'Nama Kabupaten',
			'propinsi_nama'			=> 'Nama Provinsi'
			
		); 
		$this->load->model("mdl_verifikasi","M");

		//load js..
		$this->js_plugins = array(
			'plugins/bootstrap/bootstrap-datepicker.js',
			'plugins/bootstrap/bootstrap-file-input.js',
			'plugins/datatables/jquery.dataTables.min.js',
			'plugins/bootstrap/bootstrap-select.js'
		);
	}

	
	function _reset(){
		$this->sCfg['search'] = array(
			'class'		=> $this->_getClass(),
			'date_start'=> '',
			'date_end'	=> '',
			'status'	=> '',
			'order_by'  => 'kab_kode',
			'order_dir' => 'DESC',
			'colum'		=> '',
			'keyword'	=> ''
		);
		$this->_releaseSession();
	}

	function index(){

		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		$par_filter = array(
				"offset"	=> $this->uri->segment($this->uri_segment),
			);
		$this->data_table = $this->M->cek_ganda($par_filter);
//		debugCode($this->data_table);
		$data = $this->_data(array(
				"base_url"	=> $this->own_link.'/index'
			));
		$this->_v($this->folder_view.$this->prefix_view,$data);
	}

	function search(){

		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		if($this->input->post('btn_search')){

			if($this->input->post('colum') && trim($this->input->post('colum'))!="")
				$this->jCfg['search']['colum'] = $this->input->post('colum');
			else
				$this->jCfg['search']['colum'] = "";	

			if($this->input->post('keyword') && trim($this->input->post('keyword'))!="")
				$this->jCfg['search']['keyword'] = $this->input->post('keyword');
			else
				$this->jCfg['search']['keyword'] = "";

			$this->_releaseSession();
		}

		if($this->input->post('btn_reset')){
			$this->_reset();
		}

//		$this->per_page = 50;

		$par_filter = array(
				"offset"	=> $this->uri->segment($this->uri_segment),
				"propinsi"	=> $this->input->post('propinsi'),
				"param"		=> $this->cat_search
			);
		$this->data_table = $this->M->verifikasi($par_filter);
		$data = $this->_data(array(
				"base_url"	=> $this->own_link.'/index'
			));
		$data['param'] = array(
			"propinsi" => trim($this->input->post('propinsi'))
		);
		$this->_v($this->folder_view.$this->prefix_view,$data);
	}
	

	function add(){	
		$this->breadcrumb[] = array(
				"title"		=> "Add"
			);		
		$this->_v($this->folder_view.$this->prefix_view."_form",array());
	}

	function edit(){

		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'kta_id'	=> $id
				));			
			$this->_v($this->folder_view.$this->prefix_view."_form",array());
		}else{
			redirect($this->own_link);
		}
	}
	
	function delete(){
		$id=_decrypt(dbClean(trim($this->input->get('_id'))));
		if(trim($id) != ''){
			$o = $this->DATA->_delete(
				array("kta_id"	=> idClean($id)),
				TRUE
			);
			
		}
		redirect($this->own_link."?msg=".urldecode('Data KTA ganda berhasil dihapus')."&type_msg=success");
	}

	function save(){
		$verifikasi = array_combine($_POST['kab_kode'],$_POST['ppd2015']);
		foreach ((array)$verifikasi as $k => $v) {
			$ppd2016 = $v + (($v / 100) * 2); 
			$ppd2017 = $ppd2016 + (($ppd2016 / 100) * 2); 
			$ver	 = ($ppd2017 / 1000) + 1; 
			if($ppd2017 <= 1000000){
				$minver	 = round($ver,1) + ((round($ver,1) / 100) * 50) + 1;				
			}else{
				$minver	 = 1500;								
			}			
			$data = array(
				'pdd2015'				=> $v,
				'pdd2016'				=> $ppd2016,
				'pdd2017'				=> $ppd2017,
				'kab_min_verifikasi'	=> $minver				
			);		
			$a = $this->_save_master( 
				$data,
				array(
					'kab_kode' => $k
				),
				$k
			);
		}			
		redirect($this->own_link."/edit/?_id="._encrypt($_POST['prop_kode'])."&msg=".urldecode('Tabulasi Jumlah KTA Per Kabupaten telah diupdate.')."&type_msg=success");
//		redirect($th
	}
}